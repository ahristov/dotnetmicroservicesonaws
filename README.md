# Build Microservices with Dot.Net on AWS

This project contains code from studying [Build Microservices with Dot.Net on AWS](https://www.udemy.com/build-microservices-with-aspnet-core-amazon-web-services) course.

## Notes

Well designed AWS:

- big ball of mud: A project so big, that no developer knows the entire code base

- data offloading: Key data of the service must be stored in a shared persistent store

- AWS component for data offloading: Memcached (a part of Elastic Cache)

- AWS Cognito is: User sign-up, sign-in and access control service

- Search API vs Worker: The worker is a Lambda function, we can use them interchangeably

## Section 4: Authentication with AWS Cognito

### Authenticating users

Project specification for Authentication and Microservices

- Centralized user accounts system

- Highly available and reliable

- Support for roles: users, staff

- Authentication through OAuth and OpenId Connect

- Federation (linking with Google, Facebook, etc.)

- Should plug into ASP.Net Core Identity

- Support for token authentication (with Jwt) as well as API authentication

### Configuring AWS Cognito

You can configure **Cognito** at [Cognito](https://console.aws.amazon.com/cognito/home?region=us-east-1#)

We create "Manage User Pools", as "Identity Pools" and for access to Amazon services. We name the pool "WebAdvert".

Policies: AWS SDK does not respect the policies for password, which sounds like a bug. Thereof the same password policies have to be set up in both levels - Cognito and SDK.

In WebAdvert we have min length of 6, and not requirements for password content. Also we select _Allow users to sign themselves up_, which will create a sign-up form.

Triggers: these are about Lambda functions. To learn more, google "AWS Cognito events".

Emails: With default settings we have Cognito to send emails on our behalf. Best practices suggest that customers send emails through Amazon SES for production User Pools due to a daily email limit. [Learn more about email best practices](https://docs.aws.amazon.com/cognito/latest/developerguide/signing-up-users-in-your-app.html)

Pool Id: The `User Pool Id` is **needed later**.

App Clients: We create one or multiple app clients: "Web Client", "Android Client", "iOS Client", etc., so we have different ways of authentication for different apps.

For our app client we select:

- Generate client secret

- Enable sign-in API for server-based authentication (ADMIN_NO_SRP_AUTH)

The second option we need because the users will be authenticated from the ASP.NET backend and not from front-end (React, Angular, etc.) framework.

App client Id and secret: the `App Client Id` and `App Client Secret` are **needed later**.

### Setup AWS Credentials

In AWS console open Services dropdown and select **IAM** (Identity and access management).
Create new user that only for programmatic access to the APIs.

Once created go to "Add permissions", then to "Attach existing policies". Add the name of the service you want to use. In our case, type "cognito" in the search box, add policy "AmazonCognitoDeveloperAuthentication".

Create new access keys from the "Security" tab. The secret key is only shown at this stage and cannot be seen later. So, we need to copy and store it secure, by downloading as .csv file.

Next, we need to _add user profile to Windows_. In windows explorer type `%USERPROFILE%` to go to your user's root directory. Create folder `.aws` (type .aws. in windows explorer to allow create directory prefixed with a dot...).

Make sure Windows explorer shows file extensions. Go to `.aws` directory and create new text document without extension, named `credentials`. Make sure has no extentions. Add the access key id and secret key:

```yaml
[default]
aws_access_key_id=AKIA....
aws_secret_access_key=miPCb....
```

### ASP.Net MVC Core with Cognito

When creating the ASP.Net MVC Core application, select "no authentication" and "no https".

Install the following Nuget packages:

- Amazon.AspNetCore.Identity.Cognito

- Amazon.Extensions.CognitoAuthentication

Modify `appsettings.json`, add:

```json
  "AWS": {
    "Region": "us-east-1",
    "UserPoolClientId": "7mmg...",
    "UserPoolClientSecret": "16vq...",
    "UserPoolId": "us-east-1_OmR..."
  }
```

Modify `Startup.cs`, add `services.AddCognitoIdentity();` and `app.UseAuthentication();` as follows:

```csharp
        public void ConfigureServices(IServiceCollection services)
        {
...        	
            // services.AddCognitoIdentity();
            services.AddCognitoIdentity(config => {
                config.Password = new Microsoft.AspNetCore.Identity.PasswordOptions
                {
                    RequireDigit = false,
                    RequiredLength = 6,
                    RequiredUniqueChars = 0,
                    RequireLowercase = false,
                    RequireNonAlphanumeric = false,
                    RequireUppercase = false,
                };
            });
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
...
            app.UseCookiePolicy();
            app.UseAuthentication();
...
```

Note: If necessary, manually specify the PasswordOpions, as the SDK currently does noit fetch the password policies from the AWS console.



## Links

[Build Microservices with Dot.Net on AWS](https://www.udemy.com/build-microservices-with-aspnet-core-amazon-web-services)
